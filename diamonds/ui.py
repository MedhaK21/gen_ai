import pygame

def draw_welcome_screen(screen):
    screen.fill((0, 128, 0))  # Green background
    font = pygame.font.Font(None, 36)
    text = font.render("Welcome to Diamonds Game", True, (255, 255, 255))  # Render the welcome message
    text_rect = text.get_rect(center=(screen.get_width() // 2, screen.get_height() // 2 - 100))  # Center the text on the screen
    screen.blit(text, text_rect)  # Draw the text on the screen

    # Draw the card for 2-player game
    card_image = pygame.image.load("images/twoplayers.jpg")
    card_image = pygame.transform.scale(card_image, (150, 200))
    screen.blit(card_image, (screen.get_width() // 4 - 75, screen.get_height() // 2))

    # Draw the card for 3-player game
    card_image = pygame.image.load("images/threeplayers.png")
    card_image = pygame.transform.scale(card_image, (150, 200))
    screen.blit(card_image, (3 * screen.get_width() // 4 - 75, screen.get_height() // 2))

    pygame.display.flip()  # Update the display


def play_2_players(screen):
    screen.fill((0, 128, 0))  # Green background
    font = pygame.font.Font(None, 36)
    text = font.render("2 Players Game", True, (255, 255, 255))
    screen.blit(text, (300, 20))
    pygame.display.flip()

def play_3_players(screen):
    screen.fill((0, 128, 0))  # Green background
    font = pygame.font.Font(None, 36)
    text = font.render("3 Players Game", True, (255, 255, 255))
    screen.blit(text, (300, 20))
    pygame.display.flip()

def draw_game(screen, player1, player2, revealed_diamond, computer_card, winner_message=None, same_card=None):
    screen.fill((0, 128, 0))  # Green background
    font = pygame.font.Font(None, 36)
    text = font.render("Diamond Card Game", True, (255, 255, 255))
    screen.blit(text, (300, 20))

    # Calculate the width and height of a card based on screen dimensions
    card_width = screen.get_width() // 15  # Adjust the divisor as needed
    card_height = card_width * 3 // 2  # Assuming standard card aspect ratio

    # Draw player1's hand
    for i, card in enumerate(player1.hand):
        card_image = pygame.image.load(f"images/{card.rank}{card.suit}.png")
        card_image = pygame.transform.scale(card_image, (card_width, card_height))
        screen.blit(card_image, (50 + i * (card_width + 5), 400))

    # Draw player2's hand (show only back of cards)
    card_back_image = pygame.image.load("images/card_back.png")
    for i in range(len(player2.hand)):
        card_back_image = pygame.transform.scale(card_back_image, (card_width, card_height))
        screen.blit(card_back_image, (50 + i * (card_width + 5), 50))

    # Draw revealed diamond if available
    if revealed_diamond:
        diamond_image = pygame.image.load(f"images/{revealed_diamond.rank}D.png")
        diamond_image = pygame.transform.scale(diamond_image, (card_width, card_height))
        screen.blit(diamond_image, (300, 200))

    # Draw computer's card choice
    if computer_card:
        if same_card:
            card_image = pygame.image.load(f"images/{same_card.rank}{same_card.suit}.png")
        else:
            card_image = pygame.image.load(f"images/{computer_card.rank}{computer_card.suit}.png")
        card_image = pygame.transform.scale(card_image, (card_width, card_height))
        screen.blit(card_image, (350, 50))  # Adjust position as needed

    # Draw scores
    font = pygame.font.Font(None, 24)
    player1_score_text = font.render(f"{player1.name}: {player1.score}", True, (255, 255, 255))
    player2_score_text = font.render(f"{player2.name}: {player2.score}", True, (255, 255, 255))
    screen.blit(player1_score_text, (50, 360))
    screen.blit(player2_score_text, (50, 20))

    # Draw winner message if available
    if winner_message:
        winner_text = font.render(winner_message, True, (255, 255, 255))
        screen.blit(winner_text, (300, 300))

    pygame.display.flip()
import pygame


def handle_input(player1):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            # Check if the click is on one of the player's cards
            for i, card in enumerate(player1.hand):
                card_width = pygame.display.get_surface().get_width() // 15
                card_height = card_width * 3 // 2
                if 50 + i * (card_width + 5) <= x <= 50 + (i + 1) * (card_width + 5) and 400 <= y <= 400 + card_height:
                    selected_card = player1.hand.pop(i)
                    return selected_card
    return None


