import pygame
import random

class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return f"{self.rank}{self.suit}"


class Deck:
    def __init__(self, suit):
        self.cards = [Card(suit, rank) for rank in range(2, 15)]

    def shuffle(self):
        random.shuffle(self.cards)

    def deal(self, num_cards):
        return self.cards[:num_cards]


class Player:
    def __init__(self, name, suit):
        self.name = name
        self.suit = suit
        self.hand = []
        self.score = 0

    def deal_cards(self, deck, num_cards):
        self.hand = deck.deal(num_cards)

    def human_bid(self):
        while True:
            card_choice = input(f"{self.name}, choose a card (e.g., 7{self.suit}): ")
            for card in self.hand:
                if str(card) == card_choice:
                    self.hand.remove(card)
                    return card
            print(f"Invalid card choice for {self.name}.")

    def computer_bid(self, revealed_diamond_rank):
        highest_card = max(self.hand, key=lambda card: card.rank)
        self.hand.remove(highest_card)
        return highest_card

    def collect_diamond(self, user_card, computer_card, highest_bidder):
        if user_card.rank == computer_card.rank:
            print("Both players played the same card.")
        else:
            highest_rank = max(user_card.rank, computer_card.rank)
            if self.name == highest_bidder:
                self.score += highest_rank

def draw_game(screen, players, revealed_diamond, computer_card, winner_message=None):
    screen.fill((0, 128, 0))
    font = pygame.font.Font(None, 36)
    text = font.render("Diamond Card Game", True, (255, 255, 255))
    screen.blit(text, (300, 20))

    card_width = screen.get_width() // 15
    card_height = card_width * 3 // 2

    for i, player in enumerate(players):
        # Draw player's hand
        for j, card in enumerate(player.hand):
            try:
                card_image = pygame.image.load(f"images/{card.rank}{card.suit}.png")
                card_image = pygame.transform.scale(card_image, (card_width, card_height))
                screen.blit(card_image, (50 + j * (card_width + 5), 400 + i * (card_height + 30)))
            except pygame.error as e:
                print(f"Error loading image for card {card}: {e}")

        # Draw player's score
        font = pygame.font.Font(None, 24)
        player_score_text = font.render(f"{player.name}: {player.score}", True, (255, 255, 255))
        screen.blit(player_score_text, (50, 360 + i * (card_height + 30)))

    if revealed_diamond:
        try:
            diamond_image = pygame.image.load(f"images/{revealed_diamond.rank}D.png")
            diamond_image = pygame.transform.scale(diamond_image, (card_width, card_height))
            screen.blit(diamond_image, (300, 200))
        except pygame.error as e:
            print(f"Error loading image for revealed diamond: {e}")

    if computer_card:
        try:
            card_image = pygame.image.load(f"images/{computer_card.rank}{computer_card.suit}.png")
            card_image = pygame.transform.scale(card_image, (card_width, card_height))
            screen.blit(card_image, (350, 50))
        except pygame.error as e:
            print(f"Error loading image for computer card: {e}")

    if winner_message:
        font = pygame.font.Font(None, 24)
        winner_text = font.render(winner_message, True, (255, 255, 255))
        screen.blit(winner_text, (300, 300))

    pygame.display.flip()

    screen.fill((0, 128, 0))
    font = pygame.font.Font(None, 36)
    text = font.render("Diamond Card Game", True, (255, 255, 255))
    screen.blit(text, (300, 20))

    card_width = screen.get_width() // 15
    card_height = card_width * 3 // 2

    for i, player in enumerate(players):
        # Draw player's hand
        for j, card in enumerate(player.hand):
            card_image = pygame.image.load(f"images/{card.rank}{card.suit}.png")
            card_image = pygame.transform.scale(card_image, (card_width, card_height))
            screen.blit(card_image, (50 + j * (card_width + 5), 400 + i * (card_height + 30)))

        # Draw player's score
        font = pygame.font.Font(None, 24)
        player_score_text = font.render(f"{player.name}: {player.score}", True, (255, 255, 255))
        screen.blit(player_score_text, (50, 360 + i * (card_height + 30)))

    if revealed_diamond:
        diamond_image = pygame.image.load(f"images/{revealed_diamond.rank}D.png")
        diamond_image = pygame.transform.scale(diamond_image, (card_width, card_height))
        screen.blit(diamond_image, (300, 200))

    if computer_card:
        card_image = pygame.image.load(f"images/{computer_card.rank}{computer_card.suit}.png")
        card_image = pygame.transform.scale(card_image, (card_width, card_height))
        screen.blit(card_image, (350, 50))

    if winner_message:
        font = pygame.font.Font(None, 24)
        winner_text = font.render(winner_message, True, (255, 255, 255))
        screen.blit(winner_text, (300, 300))

    pygame.display.flip()


def handle_input(player):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            card_width = screen.get_width() // 15
            card_height = card_width * 3 // 2
            for i, card in enumerate(player.hand):
                if 50 + i * (card_width + 5) <= x <= 50 + (i + 1) * (card_width + 5) and 400 <= y <= 400 + card_height:
                    selected_card = player.hand.pop(i)
                    return selected_card
    return None


def play_game(screen):
    player_names = [input(f"Enter name for Player {i+1}: ") for i in range(3)]
    player_suits = random.sample(["S", "H", "C"], 3)
    players = [Player(name, suit) for name, suit in zip(player_names, player_suits)]

    deck1 = Deck(players[0].suit)
    deck2 = Deck(players[1].suit)
    deck3 = Deck(players[2].suit)
    diamond_deck = Deck("D")
    diamond_deck.shuffle()

    deck1.shuffle()
    deck2.shuffle()
    deck3.shuffle()
    for player, deck in zip(players, [deck1, deck2, deck3]):
        player.deal_cards(deck, 13)

    winner_message = None
    while diamond_deck.cards and all(player.hand for player in players):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        draw_game(screen, players, diamond_deck.cards[-1], None, winner_message)

        selected_card = handle_input(players[0])
        if selected_card:
            player_cards = [selected_card]
            for i in range(1, len(players)):
                if players[i].name != "Computer":
                    player_cards.append(players[i].computer_bid(diamond_deck.cards[-1].rank))

            highest_bidder = max(players, key=lambda player: player_cards[players.index(player)].rank).name

            if len(set(card.rank for card in player_cards)) == 1:
                diamond_card = diamond_deck.deal(1)[0]
                split_score = player_cards[0].rank // 3
                for player in players:
                    player.score += split_score
                winner_message = "All players played the same card."
            else:
                for player in players:
                    player.collect_diamond(player_cards[0], player_cards[players.index(player)], highest_bidder)
                winner_message = f"{highest_bidder} wins the trick!"

            draw_game(screen, players, diamond_deck.cards[-1], None, winner_message)
            pygame.time.delay(2000)

            for i, player in enumerate(players):
                print(f"{player.name} chose: {player_cards[i]}")

            for player in players:
                print(f"{player.name}: {player.score}")

    winner_message = max(players, key=lambda player: player.score)
    draw_game(screen, players, diamond_deck.cards[-1], None, f"{winner_message.name} wins the game with a score of {winner_message.score}!")
    pygame.time.delay(4000)
    print(f"{winner_message.name} wins the game with a score of {winner_message.score}!")
    print("Final Scores:")
    for player in players:
        print(f"{player.name}: {player.score}")


if __name__ == "__main__":
    pygame.init()
    screen = pygame.display.set_mode((800, 800))
    pygame.display.set_caption("Diamond Card Game")
    play_game(screen)
