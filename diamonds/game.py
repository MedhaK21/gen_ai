import pygame
import random
from ui import draw_game,handle_input
from strategies import Deck, Player

def play_game_2_players(screen):
    # Setup players and decks
    player1_name = input("Enter Player 1's name: ")
    player1 = Player(player1_name, random.choice(["S", "H", "C"]))
    player2_name = input("Enter Player 2's name: ")
    player2 = Player(player2_name, list({"S", "H", "C"} - {player1.suit})[0])

    # Setup decks
    deck1 = Deck(player1.suit)
    deck2 = Deck(player2.suit)
    diamond_deck = Deck("D")
    diamond_deck.shuffle()

    # Deal cards
    deck1.shuffle()
    deck2.shuffle()
    player1.deal_cards(deck1)
    player2.deal_cards(deck2)

    # Main game loop
    winner_message = None
    while diamond_deck.cards and player1.hand and player2.hand:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        draw_game(screen, player1, player2, diamond_deck.cards[-1], None, winner_message)  # Pass None for computer_card
        selected_card = handle_input(player1)
        if selected_card:
            # Player selected a card, handle the game logic
            computer_card = player2.computer_bid(diamond_deck.cards[-1].rank)
            if selected_card.rank == computer_card.rank:
                # Both players played cards of the same rank
                diamond_card = diamond_deck.deal(1)[0]
                split_score = computer_card.rank // 2  # Calculate the split score
                player1.score += split_score  # Add half of the diamond card's rank to player 1's score
                player2.score += split_score  # Add half of the diamond card's rank to player 2's score
                winner_message = "Both players played the same card."
            else:
                winner = player1 if selected_card.rank > computer_card.rank else player2
                winner_message = f"{winner.name} wins the trick!"
                winner.collect_diamond(selected_card, computer_card)  # Pass both cards to collect_diamond method

            # Display updated scores
            draw_game(screen, player1, player2, diamond_deck.cards[-1], computer_card, winner_message)

            # Introduce a short delay before proceeding to the next iteration
            pygame.time.delay(2000)  # Delay for 2 seconds (adjust as needed)

            # Print both players' card choices
            print(f"{player1.name} chose: {selected_card}")
            print(f"{player2.name} chose: {computer_card}")

            # Print scores
            print(f"Scores: {player1.name}: {player1.score}, {player2.name}: {player2.score}")

    # Determine and display the winner message
    winner_message = f"{player1.name} wins the game with a score of {player1.score}!" if player1.score > player2.score else f"{player2.name} wins the game with a score of {player2.score}!"
    draw_game(screen, player1, player2, None, None, winner_message)
    # Pass None for both revealed_diamond and computer_card
    pygame.time.delay(4000)
    print(winner_message)
    print("Final Scores:")
    print(f"{player1.name}: {player1.score}")
    print(f"{player2.name}: {player2.score}")
    return winner_message


def play_game_3_players(screen):
    # Setup players and decks
    player1_name = input("Enter Player 1's name: ")
    player1 = Player(player1_name, random.choice(["S", "H", "C"]))
    player2_name = input("Enter Player 2's name: ")
    player2 = Player(player2_name, list({"S", "H", "C"} - {player1.suit})[0])
    player3_name = input("Enter Player 3's name: ")
    player3 = Player(player3_name, list({"S", "H", "C"} - {player1.suit, player2.suit})[0])

    # Setup decks
    deck1 = Deck(player1.suit)
    deck2 = Deck(player2.suit)
    deck3 = Deck(player3.suit)
    diamond_deck = Deck("D")
    diamond_deck.shuffle()

    # Deal cards
    deck1.shuffle()
    deck2.shuffle()
    deck3.shuffle()
    player1.deal_cards(deck1)
    player2.deal_cards(deck2)
    player3.deal_cards(deck3)

    # Main game loop
    winner_message = None
    while diamond_deck.cards and player1.hand and player2.hand and player3.hand:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        draw_game(screen, player1, player2, player3, diamond_deck.cards[-1], None, winner_message)  # Pass None for computer_card
        selected_card = handle_input(player1)
        if selected_card:
            # Player selected a card, handle the game logic
            computer_card = player2.computer_bid(diamond_deck.cards[-1].rank)
            if selected_card.rank == computer_card.rank:
                # Both players played cards of the same rank
                diamond_card = diamond_deck.deal(1)[0]
                split_score = computer_card.rank // 2  # Calculate the split score
                player1.score += split_score  # Add half of the diamond card's rank to player 1's score
                player2.score += split_score  # Add half of the diamond card's rank to player 2's score
                winner_message = "Both players played the same card."
            else:
                winner = player1 if selected_card.rank > computer_card.rank else player2
                winner_message = f"{winner.name} wins the trick!"
                winner.collect_diamond(selected_card, computer_card)  # Pass both cards to collect_diamond method

            # Display updated scores
            draw_game(screen, player1, player2, player3, diamond_deck.cards[-1], computer_card, winner_message)

            # Introduce a short delay before proceeding to the next iteration
            pygame.time.delay(2000)  # Delay for 2 seconds (adjust as needed)

            # Print both players' card choices
            print(f"{player1.name} chose: {selected_card}")
            print(f"{player2.name} chose: {computer_card}")

            # Print scores
            print(f"Scores: {player1.name}: {player1.score}, {player2.name}: {player2.score}")

    # Determine and display the winner message
    winner_message = f"{player1.name} wins the game with a score of {player1.score}!" if player1.score > player2.score else f"{player2.name} wins the game with a score of {player2.score}!"
    draw_game(screen, player1, player2, player3, None, None, winner_message)
    # Pass None for both revealed_diamond and computer_card
    pygame.time.delay(4000)
    print(winner_message)
    print("Final Scores:")
    print(f"{player1.name}: {player1.score}")
    print(f"{player2.name}: {player2.score}")
    print(f"{player3.name}: {player3.score}")
    return winner_message
