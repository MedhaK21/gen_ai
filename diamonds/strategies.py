import random

class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return f"{self.rank}{self.suit}"


class Deck:
    def __init__(self, suit):
        self.cards = [Card(suit, rank) for rank in range(2, 15)]

    def shuffle(self):
        random.shuffle(self.cards)

    def deal(self, num_cards):
        return self.cards[:num_cards]


class Player:
    def __init__(self, name, suit):
        self.name = name
        self.suit = suit
        self.hand = []
        self.score = 0

    def deal_cards(self, deck):
        self.hand = deck.deal(13)

    def computer_bid(self, revealed_diamond_rank):
        # Find the highest card in the hand
        highest_card = max(self.hand, key=lambda card: card.rank)
        self.hand.remove(highest_card)
        return highest_card

    def collect_diamond(self, user_card, computer_card):
        if user_card.rank == computer_card.rank:
            print("Both players played the same card.")
        else:
            highest_rank = max(user_card.rank, computer_card.rank)
            self.score += highest_rank  # Update the score based on the highest rank bid
